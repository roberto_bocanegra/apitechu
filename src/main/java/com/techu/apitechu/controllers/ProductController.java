package com.techu.apitechu.controllers;

import com.techu.apitechu.ApitechuApplication;
import com.techu.apitechu.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductController {

    static final String APIBaseUrl = "/apitechu/v1";

    @GetMapping(APIBaseUrl + "/products")
    public ArrayList<ProductModel> getProducts() {
        System.out.println("getProducts");

        return ApitechuApplication.productModels;
    }
    @GetMapping(APIBaseUrl + "/products/{id}")
    public ProductModel getProductById(@PathVariable String id) {
        System.out.println("getProductById");
        System.out.println("la id del producto a obtener es " + id);

        ProductModel result = new ProductModel();

        for (ProductModel product : ApitechuApplication.productModels){
            if (product.getId().equals(id)){
                System.out.println("Producto encontrado");
                result = product;
            }

        }

        return result;
    }
    @PostMapping(APIBaseUrl + "/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct){
        System.out.println("createProduct");
        System.out.println("La          id del producto a crear es " + newProduct.getId());
        System.out.println("La descripcion del producto a crear es " + newProduct.getDesc());
        System.out.println("El      precio del producto a crear es " + newProduct.getPrice());

        ApitechuApplication.productModels.add(newProduct);

        return newProduct;
    }

    @PutMapping(APIBaseUrl + "/products/{id}")
    public ProductModel updateProduct(@RequestBody ProductModel product, @PathVariable String id) {
        System.out.println("UpdateProduct");
        System.out.println("La          id del producto a actualizar en parametro URL es " + id);
        System.out.println("La          id del producto a actualizar es " + product.getId());
        System.out.println("La descripcion del producto a actualizar es " + product.getDesc());
        System.out.println("El      precio del producto a actualizar es " + product.getPrice());

        for (ProductModel productInList : ApitechuApplication.productModels) {
            if (productInList.getId().equals(id)) {
                productInList.setId(product.getId());
                productInList.setDesc(product.getDesc());
                productInList.setPrice(product.getPrice());
            }
        }

        return product;
    }
    //mioooooooo
    @PatchMapping(APIBaseUrl + "/products/{id}")
    public ProductModel PatchProduct(@RequestBody ProductModel productData, @PathVariable String id) {

        System.out.println("PatchProduct");
        ProductModel result = new ProductModel();

        for (ProductModel productInList : ApitechuApplication.productModels) {
            if (productInList.getId().equals(id)) {

                System.out.println("producto encontrado");
                result = productInList;

                if (productData.getDesc() != null){
                    System.out.println("La nueva descripcion del producto a PATCHar es: " + productData.getDesc());
                    productInList.setDesc(productData.getDesc());
                }

                if (productData.getPrice() > 0){
                    System.out.println("El nuevo      precio del producto a PATCHar es: " + productData.getPrice());
                    productInList.setPrice(productData.getPrice());
                }
            }
        }

        return result;
    }

    @DeleteMapping(APIBaseUrl + "/products/{id}")
    public ProductModel deleteProduct(@PathVariable String id) {
        System.out.println("deleteProduct");
        System.out.println("la id del producto a borrar es : " + id);

        ProductModel result = new ProductModel();
        boolean foundCompany = false;
        for (ProductModel productInList : ApitechuApplication.productModels) {
            if (productInList.getId().equals(id)){
                System.out.println("producto encontrado");
                foundCompany = true;
                result = productInList;
            }
        }
        if (foundCompany){
            System.out.println("Borrando producto");
            ApitechuApplication.productModels.remove(result);
        }

        return result;
    }
}
